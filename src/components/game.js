import React from 'react';
import Board from './board'

class Game extends React.Component {

    calculateWinner(squares) {
      const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
      ];
      for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
          console.log(squares[a]);
          return "Winner: " + squares[a];
        }
      }
      let counter = 0;
      for(let i = 0; i < squares.length; i++){
        
        if(squares[i] === 'X' || squares[i] === 'O'){
            counter++;
        }
        //console.log("Counter is at: ")
        //console.log(counter)
        if(counter >= 9){
          return "End in draw."
        }
      }
      return null;
    }

    render() {
      return (
        <div className="game">
          <div className="game-board">
            <Board winnerFunc={this.calculateWinner}/>
          </div>
          <div className="game-info">
            <div>{/* status */}</div>
            <ol>{/* TODO */}</ol>
          </div>
        </div>
      );
    }
  }

  export default Game;