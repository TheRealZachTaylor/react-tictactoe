import React from 'react';
import Square from './square';
import Row from './row';

class Board extends React.Component {

    constructor(props){
        super(props)
        this.props.winnerFunc.bind(this)
        this.state = {
            squares: Array(9).fill(null),
            xIsNext: null,
        }

    }

    handleClick(i){
        const squares = this.state.squares.slice();

        if(this.props.winnerFunc(squares) || squares[i]){
            return;
        }

        if(this.state.xIsNext){
            squares[i] = 'X';
        } else {
            squares[i] = 'O';
        }
        /*
            NOTE: This can be done with a ternary operator;
            I'm not too familiar, so I left it with a simple
            if statement for my own readability later.
            squares[i] = this.state.xIsNext ? 'X' : 'O';
        */
        this.setState({
            squares: squares,
            xIsNext: !this.state.xIsNext,
        });
    }

    renderSquare(i) {
      return (
        <Square
            value={this.state.squares[i]}
            onClick={() => this.handleClick(i)}
        />
      );
    }
  
    render() {
      const winner = this.props.winnerFunc(this.state.squares);
      let status;
      if(winner){
        status = winner;
      } else {
        if(this.state.xIsNext === null){
          return (
            <div>
              <button onClick={
                () => this.setState({
                  xIsNext: true
              })}>
                Start Player X
              </button>
              <button onClick={
                () => this.setState({
                  xIsNext: false
              })}>
                Start Player O
              </button>
            </div>
          )
        } else {
          if(this.state.xIsNext){
            status = "Next player: X";
          } else {
            status = "Next player: O";
          }
        } 
      }
  
      return (
        <div>
          <div className="board-row">
            {this.renderSquare(0)}
            {this.renderSquare(1)}
            {this.renderSquare(2)}
          </div>
          <div className="board-row">
            {this.renderSquare(3)}
            {this.renderSquare(4)}
            {this.renderSquare(5)}
          </div>
          <div className="board-row">
            {this.renderSquare(6)}
            {this.renderSquare(7)}
            {this.renderSquare(8)}
          </div>
          <div className="status">{status}</div>
        </div>
      );
    }
  }

export default Board;