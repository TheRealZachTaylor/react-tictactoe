import React from 'react';
import { shallow, mount, render } from 'enzyme';

import Square from 'src/components/square'

describe('A square', () => {
    it('should render without errors', () => {
        const square = shallow(<Square />);
        const button = ‘<button className=“square” onClick={props.onClick}>{props.value}</button>’;
        expect(square)
        .contains(button)
        .toBe(true);
    });
});
